<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\StudentApiController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
Route::post('senjata', [SenjataApiController::class,'store']);
Route::get('senjata', [SenjataApiController::class,'index']);
Route::put('senjata/{id}', [SenjataApiController::class,'update']);
Route::delete('senjata/{id}', [SenjataApiController::class,'destroy']);
