<?php

namespace App\Http\Controllers;

use App\Models\Senjata;
use Illuminate\Http\Request;
use File;
use Validator;

class SenjataApiController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $senjata = Senjata::all()->toJson(JSON_PRETTY_PRINT);
        return response($senjata, 200);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $validateData = Validator::make($request->all(), [
            'nama' => 'required|min:3|max:50',
            'no_seri' => 'required',
            'kapasitas' => 'required',
            'ukuran' => 'required',
            'berat' => 'required',
            'berat' => 'required',
            'daya_tembak' => 'required',
            'kecepatan' => 'required',
            'status' => 'required',
            'pemilik' => 'required',
            'sejarah' => 'required',
            'pemakaian' => 'required',
            'image' => 'required|file|image|max:1000',
        ]);
        if ($validateData->fails()) {
            return response($validateData->errors(), 400);
        }else{
            $senjata = new Senjata();
            $senjata->nama = $validateData['nama'];
            $senjata->no_seri = $validateData['no_seri'];
            $senjata->kapasitas = $validateData['kapasitas'];
            $senjata->ukuran = $validateData['ukuran'];
            $senjata->berat = $validateData['berat'];
            $senjata->daya_tembak = $validateData['daya_tembak'];
            $senjata->kecepatan = $validateData['kecepatan'];
            $senjata->status = $validateData['status'];
            $senjata->pemilik = $validateData['pemilik'];
            $senjata->sejarah = $validateData['sejarah'];
            $senjata->pemakaian = $validateData['pemakaian'];
            if($request->hasFile('image'))
            {
                $extFile = $request->image->getClientOriginalExtension();
                $namaFile = 'user-'.time().".".$extFile;
                $path = $request->image->move('assets/image/senjata',$namaFile);
                $senjata->image = $path;
            }
            $senjata->save();
            return response()->json([
            "message" => "senjata record created"
            ], 201);
        }
           
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        if (Senjata::where('id', $id)->exists()) {
            $validateData = Validator::make($request->all(), [
                'nama' => 'required|min:3|max:50',
                'no_seri' => 'required',
                'kapasitas' => 'required',
                'ukuran' => 'required',
                'berat' => 'required',
                'daya_tembak' => 'required',
                'kecepatan' => 'required',
                'status' => 'required',
                'pemilik' => 'required',
                'sejarah' => 'required',
                'pemakaian' => 'required',
                'image' => 'file|image|max:1000',
        ]);
        if ($validateData->fails()) {
            return response($validateData->errors(), 400);
        }else{
            $senjata->nama = $validateData['nama'];
            $senjata->no_seri = $validateData['no_seri'];
            $senjata->kapasitas = $validateData['kapasitas'];
            $senjata->ukuran = $validateData['ukuran'];
            $senjata->berat = $validateData['berat'];
            $senjata->daya_tembak = $validateData['daya_tembak'];
            $senjata->kecepatan = $validateData['kecepatan'];
            $senjata->status = $validateData['status'];
            $senjata->pemilik = $validateData['pemilik'];
            $senjata->sejarah = $validateData['sejarah'];
            $senjata->pemakaian = $validateData['pemakaian'];
            if($request->hasFile('image'))
            {
                $extFile = $request->image->getClientOriginalExtension();
                $namaFile = 'user-'.time().".".$extFile;
                File::delete($senjata->image);
                $path = $request->image->move('assets/images',$namaFile);
                $senjata->image = $path;
            }
            $senjata->save();
            return response()->json([
            "message" => "senjata record updated"
            ], 201);
        }
        } else {
            return response()->json([
            "message" => "Senjata not found"
            ], 404);
        }           
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        if (Senjata::where('id', $id)->exists()) {
            $senjata = Senjata::find($id);
            File::delete($senjata->image);
            $senjata->delete();
            return response()->json([
            "message" => "senjata record deleted"
            ], 201);
        } else {
            return response()->json([
            "message" => "Senjata not found"
            ], 404);
            }
           
    }
}
